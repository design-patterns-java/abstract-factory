# Design Patterns OOP with Java
## _The Abstract Factory_

This project implements a simplified example for design pattern  Abstract Factory  

## Problem

Similar to Factory Method, but complexity is increased. Imagine a car factory. 
It has two assembly lines: engine and car. The car needs an engine, but the engine depends on the type of car that is created. 
An ordinary car needs an ordinary engine. Common motor has its own characteristics. 

_How resolve this?_ 

## Example

You needs of a new common car...

```java
 Car car = new CarFactory().getCar(CarType.COMMON); // assembly a common car
```
Common car needs of a new common engine... Then CarFactory imports EngineFactory (or receive by dependency injection)

```java
Engine engine = engineFactory.getEngine(carType); // assmebly a new common engine to use in CarFactory
```

But, you call in one line: 


```java
  Car car = new CarFactory().getCar(CarType.COMMON); 
```

