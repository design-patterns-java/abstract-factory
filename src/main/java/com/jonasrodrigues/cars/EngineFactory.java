package com.jonasrodrigues.cars;

class EngineFactory {
        protected Engine getEngine(CarType type)  {
            Object engine = GetObject.get(type.getClassName().concat("Engine"));
            if ( engine != null && engine instanceof Engine) {
                return (Engine) engine;
            }
        System.out.println(type.getClassName() + " should be implements Engine");
        return null;
    }
}
