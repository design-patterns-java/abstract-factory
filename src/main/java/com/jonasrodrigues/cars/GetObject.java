package com.jonasrodrigues.cars;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class GetObject {

    public static Object get(String className) {
        return get(className, null);
    }

    public static Object get(String className, Object args) {
        Constructor<?> constructor;
        try {
            Class<?> aClass = Class.forName(GetObject.class.getPackage().getName().concat("." + className));
            if (args == null) {
                constructor = aClass.getDeclaredConstructors()[0];
                return constructor.newInstance();
            } else {
                constructor = aClass.getDeclaredConstructors()[0];
                return constructor.newInstance(args);
            }
        } catch (ClassNotFoundException e) {
            System.out.println("The class " + className + " not found!");
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            System.out.println("Constructor of " + className + "not found");
        }
        return null;
    }}
