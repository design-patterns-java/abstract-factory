package com.jonasrodrigues.cars;

public interface Engine {
    void turnOn();
    void turnOff();
    int getRpm();
    void setRpm(int rpm);

    boolean status();
}
