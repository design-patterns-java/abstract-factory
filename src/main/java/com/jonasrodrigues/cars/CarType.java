package com.jonasrodrigues.cars;

public enum CarType {
    SPORT("Sport"),
    COMMON("Common");

    private String className;

    CarType(String className) {
        this.className = className;
    }

    public String getClassName() {
        return this.className;
    }
}
