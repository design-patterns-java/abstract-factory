package com.jonasrodrigues.cars;

import java.lang.reflect.InvocationTargetException;

public class CarFactory {

    EngineFactory engineFactory;

    public CarFactory() {
        this.engineFactory = new EngineFactory();
    }

    public Car getCar(CarType carType)  {
        Engine engine = engineFactory.getEngine(carType);
        Object objectCar = GetObject.get(carType.getClassName().concat("Car"), engine);
        if (objectCar != null && objectCar instanceof Car) {
            Car car =  (Car) objectCar;
            System.out.println(car.getStatus());
        }
        return null;
    }

}
