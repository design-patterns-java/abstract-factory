package com.jonasrodrigues.cars;

public interface Car {

    void turnOn();

    void turnOff();

    void accelerate();

    int getMaxVelocity();

    int getMaxFactorAcceleration();

    String getStatus();

}
