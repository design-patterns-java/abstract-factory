package com.jonasrodrigues.cars;

class CommonEngine implements Engine {

    private boolean status = false;
    private int rpm;

    protected CommonEngine() {

    }

    @Override
    public void turnOn() {
        status = true;
    }

    @Override
    public void turnOff() {
        status = false;
    }

    @Override
    public int getRpm() {
        return rpm;
    }

    @Override
    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    @Override
    public boolean status() {
        return status;
    }


}
