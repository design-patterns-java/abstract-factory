package com.jonasrodrigues.cars;

public class CommonCar implements Car{

    private static final int MAX_VELOCITY = 120;
    private static final int MAX_ACCELERATION_FACTOR = 3;


    private Engine engine;

    protected CommonCar(Engine engine ) {
        this.engine = engine;
    }


    @Override
    public void turnOn() {
        engine.turnOn();
    }

    @Override
    public void turnOff() {

    }

    @Override
    public void accelerate() {

    }

    @Override
    public int getMaxVelocity() {
        return 0;
    }

    @Override
    public int getMaxFactorAcceleration() {
        return 0;
    }

    @Override
    public String getStatus() {
        return "Engine is " + (engine.status() ? "on" : "off") + " and this car has max velocity of " + MAX_VELOCITY + "KM/h";
    }


}
