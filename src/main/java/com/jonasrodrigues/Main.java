package com.jonasrodrigues;

import com.jonasrodrigues.cars.Car;
import com.jonasrodrigues.cars.CarFactory;
import com.jonasrodrigues.cars.CarType;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Car car = new CarFactory().getCar(CarType.COMMON);
    }
}